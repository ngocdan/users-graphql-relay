import graphql from "babel-plugin-relay/macro";

const deleteUserMutation = graphql`
  mutation deleteUserMutation($id: uuid) {
    delete_user(where: { id: { _eq: $id } }) {
      affected_rows
      returning {
        id
        username
      }
    }
  }
`;

export { deleteUserMutation };
