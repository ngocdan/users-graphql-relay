/**
 * @generated SignedSource<<1894e43db35bed3f4a87208ee0f92e94>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Mutation } from 'relay-runtime';
export type updateUserMutation$variables = {|
  id?: ?any,
  username?: ?string,
  role?: ?string,
  username_id?: ?string,
  password?: ?string,
|};
export type updateUserMutation$data = {|
  +update_user: ?{|
    +affected_rows: number,
    +returning: $ReadOnlyArray<{|
      +id: any,
      +username: string,
      +role: string,
      +username_id: string,
      +password: string,
    |}>,
  |},
|};
export type updateUserMutation = {|
  variables: updateUserMutation$variables,
  response: updateUserMutation$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "id"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "password"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "role"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "username"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "username_id"
},
v5 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "password",
            "variableName": "password"
          },
          {
            "kind": "Variable",
            "name": "role",
            "variableName": "role"
          },
          {
            "kind": "Variable",
            "name": "username",
            "variableName": "username"
          },
          {
            "kind": "Variable",
            "name": "username_id",
            "variableName": "username_id"
          }
        ],
        "kind": "ObjectValue",
        "name": "_set"
      },
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "_eq",
                "variableName": "id"
              }
            ],
            "kind": "ObjectValue",
            "name": "id"
          }
        ],
        "kind": "ObjectValue",
        "name": "where"
      }
    ],
    "concreteType": "user_mutation_response",
    "kind": "LinkedField",
    "name": "update_user",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "affected_rows",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "user",
        "kind": "LinkedField",
        "name": "returning",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "username",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "role",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "username_id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "password",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "updateUserMutation",
    "selections": (v5/*: any*/),
    "type": "mutation_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v3/*: any*/),
      (v2/*: any*/),
      (v4/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Operation",
    "name": "updateUserMutation",
    "selections": (v5/*: any*/)
  },
  "params": {
    "cacheID": "47bcea6ba53ad4640fa1474865bb9051",
    "id": null,
    "metadata": {},
    "name": "updateUserMutation",
    "operationKind": "mutation",
    "text": "mutation updateUserMutation(\n  $id: uuid\n  $username: String\n  $role: String\n  $username_id: String\n  $password: String\n) {\n  update_user(where: {id: {_eq: $id}}, _set: {username: $username, role: $role, username_id: $username_id, password: $password}) {\n    affected_rows\n    returning {\n      id\n      username\n      role\n      username_id\n      password\n    }\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "9008e2aef5da6f262448e57144c54197";

module.exports = ((node/*: any*/)/*: Mutation<
  updateUserMutation$variables,
  updateUserMutation$data,
>*/);
