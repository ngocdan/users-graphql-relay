/**
 * @generated SignedSource<<6a9ebb2ef856ba1aec9613cc98b37ef4>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Mutation } from 'relay-runtime';
export type createUserMutation$variables = {|
  username?: ?string,
  role?: ?string,
  username_id?: ?string,
  password?: ?string,
|};
export type createUserMutation$data = {|
  +insert_user: ?{|
    +affected_rows: number,
    +returning: $ReadOnlyArray<{|
      +username: string,
      +role: string,
      +username_id: string,
      +password: string,
    |}>,
  |},
|};
export type createUserMutation = {|
  variables: createUserMutation$variables,
  response: createUserMutation$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "password"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "role"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "username"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "username_id"
},
v4 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "password",
            "variableName": "password"
          },
          {
            "kind": "Variable",
            "name": "role",
            "variableName": "role"
          },
          {
            "kind": "Variable",
            "name": "username",
            "variableName": "username"
          },
          {
            "kind": "Variable",
            "name": "username_id",
            "variableName": "username_id"
          }
        ],
        "kind": "ObjectValue",
        "name": "objects"
      }
    ],
    "concreteType": "user_mutation_response",
    "kind": "LinkedField",
    "name": "insert_user",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "affected_rows",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "user",
        "kind": "LinkedField",
        "name": "returning",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "username",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "role",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "username_id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "password",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "createUserMutation",
    "selections": (v4/*: any*/),
    "type": "mutation_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v2/*: any*/),
      (v1/*: any*/),
      (v3/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "createUserMutation",
    "selections": (v4/*: any*/)
  },
  "params": {
    "cacheID": "e392c927998a284f1818c17b2469937b",
    "id": null,
    "metadata": {},
    "name": "createUserMutation",
    "operationKind": "mutation",
    "text": "mutation createUserMutation(\n  $username: String\n  $role: String\n  $username_id: String\n  $password: String\n) {\n  insert_user(objects: {username: $username, role: $role, username_id: $username_id, password: $password}) {\n    affected_rows\n    returning {\n      username\n      role\n      username_id\n      password\n    }\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "36e1ce301ba52b41ec3beaaab5834dd5";

module.exports = ((node/*: any*/)/*: Mutation<
  createUserMutation$variables,
  createUserMutation$data,
>*/);
