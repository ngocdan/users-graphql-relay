/**
 * @generated SignedSource<<bfdf8c0a98e6b1e9d053e18b69c4365a>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Mutation } from 'relay-runtime';
export type deleteUserMutation$variables = {|
  id?: ?any,
|};
export type deleteUserMutation$data = {|
  +delete_user: ?{|
    +affected_rows: number,
    +returning: $ReadOnlyArray<{|
      +id: any,
      +username: string,
    |}>,
  |},
|};
export type deleteUserMutation = {|
  variables: deleteUserMutation$variables,
  response: deleteUserMutation$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "_eq",
                "variableName": "id"
              }
            ],
            "kind": "ObjectValue",
            "name": "id"
          }
        ],
        "kind": "ObjectValue",
        "name": "where"
      }
    ],
    "concreteType": "user_mutation_response",
    "kind": "LinkedField",
    "name": "delete_user",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "affected_rows",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "user",
        "kind": "LinkedField",
        "name": "returning",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "username",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "deleteUserMutation",
    "selections": (v1/*: any*/),
    "type": "mutation_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "deleteUserMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "79c6aca8625ad5c05d729b1af4772a4a",
    "id": null,
    "metadata": {},
    "name": "deleteUserMutation",
    "operationKind": "mutation",
    "text": "mutation deleteUserMutation(\n  $id: uuid\n) {\n  delete_user(where: {id: {_eq: $id}}) {\n    affected_rows\n    returning {\n      id\n      username\n    }\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "ac3112b28ded06311aa502396935e0a5";

module.exports = ((node/*: any*/)/*: Mutation<
  deleteUserMutation$variables,
  deleteUserMutation$data,
>*/);
