import graphql from "babel-plugin-relay/macro";

const createUserMutation = graphql`
  mutation createUserMutation(
    $username: String
    $role: String
    $username_id: String
    $password: String
  ) {
    insert_user(
      objects: {
        username: $username
        role: $role
        username_id: $username_id
        password: $password
      }
    ) {
      affected_rows
      returning {
        username
        role
        username_id
        password
      }
    }
  }
`;

export { createUserMutation };
