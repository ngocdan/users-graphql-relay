import graphql from "babel-plugin-relay/macro";

const userMutation = graphql`
    mutation updateUserMutation(
        $id: uuid
        $username: String
        $role: String
        $username_id: String
        $password: String
    ) {
        update_user(
            where: { id: { _eq: $id } }
            _set: {
                username: $username
                role: $role
                username_id: $username_id
                password: $password
            }
        ) {
            affected_rows
            returning {
                id
                username
                role
                username_id
                password
            }
        }
    }
`;

export { userMutation };
