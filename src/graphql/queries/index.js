import graphql from "babel-plugin-relay/macro";

const queriesGetUserSearchQuery = graphql`
    query queriesGetUserSearchQuery(
        $username: String
        $role: String
        $username_id: String
    ) {
        user_aggregate {
            aggregate {
                count
            }
        }
        user(
            where: {
                role: { _ilike: $role }
                username_id: { _ilike: $username_id }
                username: { _ilike: $username }
            }
        ) {
            id
            password
            role
            username
            username_id
        }
    }
`;
const queriesGetUserQuery = graphql`
    query queriesGetUserQuery {
        user_aggregate {
            aggregate {
                count
            }
        }
        user {
            id
            password
            role
            username
            username_id
        }
    }
`;

export { queriesGetUserSearchQuery, queriesGetUserQuery };
