/**
 * @generated SignedSource<<07d1ba7ffb8f31621073dd6fde5db50d>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type queriesGetUserSearchQuery$variables = {|
  username?: ?string,
  role?: ?string,
  username_id?: ?string,
|};
export type queriesGetUserSearchQuery$data = {|
  +user_aggregate: {|
    +aggregate: ?{|
      +count: number,
    |},
  |},
  +user: $ReadOnlyArray<{|
    +id: any,
    +password: string,
    +role: string,
    +username: string,
    +username_id: string,
  |}>,
|};
export type queriesGetUserSearchQuery = {|
  variables: queriesGetUserSearchQuery$variables,
  response: queriesGetUserSearchQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "role"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "username"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "username_id"
},
v3 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "user_aggregate",
    "kind": "LinkedField",
    "name": "user_aggregate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "user_aggregate_fields",
        "kind": "LinkedField",
        "name": "aggregate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "count",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "_ilike",
                "variableName": "role"
              }
            ],
            "kind": "ObjectValue",
            "name": "role"
          },
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "_ilike",
                "variableName": "username"
              }
            ],
            "kind": "ObjectValue",
            "name": "username"
          },
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "_ilike",
                "variableName": "username_id"
              }
            ],
            "kind": "ObjectValue",
            "name": "username_id"
          }
        ],
        "kind": "ObjectValue",
        "name": "where"
      }
    ],
    "concreteType": "user",
    "kind": "LinkedField",
    "name": "user",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "password",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "role",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username_id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "queriesGetUserSearchQuery",
    "selections": (v3/*: any*/),
    "type": "query_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Operation",
    "name": "queriesGetUserSearchQuery",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "e7fe745fa1628b811303943e8484d0a5",
    "id": null,
    "metadata": {},
    "name": "queriesGetUserSearchQuery",
    "operationKind": "query",
    "text": "query queriesGetUserSearchQuery(\n  $username: String\n  $role: String\n  $username_id: String\n) {\n  user_aggregate {\n    aggregate {\n      count\n    }\n  }\n  user(where: {role: {_ilike: $role}, username_id: {_ilike: $username_id}, username: {_ilike: $username}}) {\n    id\n    password\n    role\n    username\n    username_id\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "b5d672edd64cbe3e53411fe58b1be8cd";

module.exports = ((node/*: any*/)/*: Query<
  queriesGetUserSearchQuery$variables,
  queriesGetUserSearchQuery$data,
>*/);
