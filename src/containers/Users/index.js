import React, { useEffect, useMemo, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { QueryRenderer } from "react-relay";
import TableUser from "../../components/users/Table";
import environment from "../../Enviroment";
import { queriesGetUserSearchQuery } from "../../graphql/queries";
import "./styles/index.scss";

const Users = ({ props, retry }) => {
    const [form, setForm] = useState();
    const [submit, setSubmit] = useState(false);
    const [isReload, setIsReload] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [userList, setUserList] = useState([]);

    let PageSize = 10;

    /**
     * handle slice users data to pagination
     */
    const currentTableData = useMemo(() => {
        const firstPageIndex = (currentPage - 1) * PageSize;
        const lastPageIndex = firstPageIndex + PageSize;
        return userList?.slice(firstPageIndex, lastPageIndex);
    }, [currentPage, userList]);

    useEffect(() => {
        setUserList(props?.user);
    }, [props?.user]);

    /**
     * handle change input form search
     */
    const handleChange = (e) => {
        let { name, value } = e.target;
        setForm({
            ...form,
            [name]: value,
        });
        if ([name].value === "") {
            setSubmit(false);
        }
    };

    /**
     * handle search button
     */
    const onSubmitSearch = () => {
        setCurrentPage(1);
        setSubmit(true);
    };

    useEffect(() => {
        if (isReload) {
        }
    }, [isReload]);

    /**
     * handle clear button
     */
    const onClear = () => {
        setSubmit(false);
        setForm({ role: "" });
        setCurrentPage(1);
    };

    const onReload = () => {
        setIsReload(!isReload);
    };

    return (
        <div>
            <Container>
                <div className="search-information py-4">
                    <form autoComplete="off">
                        <Row>
                            <Col>
                                <div className="d-flex">
                                    <Form.Label
                                        htmlFor="username_id"
                                        className="w-30 mr-2"
                                    >
                                        ID
                                    </Form.Label>
                                    <Form.Control
                                        type="text"
                                        id="username_id"
                                        name="username_id"
                                        onChange={(e) => handleChange(e)}
                                        value={form?.username_id || ""}
                                    />
                                </div>
                                <div className="mt-3 d-flex">
                                    <Form.Label
                                        htmlFor="username"
                                        className="w-30 mr-2"
                                    >
                                        Username
                                    </Form.Label>
                                    <Form.Control
                                        type="text"
                                        id="username"
                                        name="username"
                                        onChange={(e) => handleChange(e)}
                                        value={form?.username || ""}
                                    />
                                </div>
                            </Col>
                            <Col className="d-flex flex-column w-100 justify-content-start">
                                <Form.Group className="d-flex align-items-center w-100">
                                    <Form.Label className="mr-2 mb-0 w-30">
                                        Role
                                    </Form.Label>
                                    <Form.Control
                                        as="select"
                                        name="role"
                                        onChange={(e) => handleChange(e)}
                                        value={form?.role}
                                    >
                                        <option value=""></option>
                                        <option value="Admin">Admin</option>
                                        <option value="Manager">Manager</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col className="d-flex align-items-end">
                                <div className="d-flex h-25 align-items-end w-100">
                                    <Button
                                        className="btn-blue w-25"
                                        onClick={() => onSubmitSearch()}
                                    >
                                        Search
                                    </Button>
                                    <Button
                                        className="btn-clear ml-2 w-25"
                                        onClick={() => onClear()}
                                    >
                                        Clear
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </form>
                </div>
                {!submit ? (
                    <TableUser
                        userList={currentTableData}
                        retry={retry}
                        reload={onReload}
                        totalElements={props?.user_aggregate?.aggregate.count}
                        currentPage={currentPage}
                        pageSize={PageSize}
                        onPageChange={(page) => setCurrentPage(page)}
                    />
                ) : (
                    <QueryRenderer
                        environment={environment}
                        query={queriesGetUserSearchQuery}
                        variables={{
                            username: form?.username || `%%`,
                            role: form?.role || `%%`,
                            username_id: form?.username_id || `%%`,
                        }}
                        render={({ error, props: propSearch }) => {
                            if (!propSearch) return <>Loading</>;

                            return (
                                <TableUser
                                    userList={propSearch?.user}
                                    reload={onReload}
                                    totalElements={
                                        props?.user_aggregate?.aggregate.count
                                    }
                                    currentPage={currentPage}
                                    pageSize={PageSize}
                                    onPageChange={(page) =>
                                        setCurrentPage(page)
                                    }
                                />
                            );
                        }}
                    />
                )}
            </Container>
        </div>
    );
};

export default Users;
