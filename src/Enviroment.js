import { Environment, Network, RecordSource, Store } from "relay-runtime";

function fetchQuery(operation, variables) {
    return fetch("https://awake-kit-42.hasura.app/v1/graphql", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "x-hasura-admin-secret":
                "ZaO2sU1fwePVWNFX0iKC71yWC23pGVpKNIrUA4r3jEJgNRbDWzCkjJNZE9MbUW6O",
        },
        body: JSON.stringify({
            query: operation.text,
            variables,
        }),
    }).then((response) => {
        return response.json();
    });
}

const environment = new Environment({
    network: Network.create(fetchQuery),
    store: new Store(new RecordSource()),
});

export default environment;
