import React from "react";
import { Button, Modal } from "react-bootstrap";
import { commitMutation } from "react-relay";
import { toast } from "react-toastify";
import environment from "../../../Enviroment";
import { deleteUserMutation } from "../../../graphql/mutations/deleteUser";

const ConfirmModal = (props) => {
    /**
     * handle delete user button
     */
    const onsubmit = async () => {
        if (props?.user?.id) {
            const variables = { id: props?.user?.id };
            commitMutation(environment, {
                mutation: deleteUserMutation,
                variables: variables,
                onCompleted: (response, errors) => {
                    if (response) {
                        props.onHide();
                        props.retry();
                        toast.success(`Đã xóa user!`, {
                            position: toast.POSITION.BOTTOM_RIGHT,
                        });
                    }
                },
                onError: (err) => {
                    toast.error(`Xóa user thất bại!`, {
                        position: toast.POSITION.BOTTOM_RIGHT,
                    });
                    console.error(err);
                },
            });
        }
    };

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header className="d-flex justify-between align-items-center">
                <Modal.Title id="contained-modal-title-vcenter">
                    Confirm delete
                </Modal.Title>
                <Button className="btn-close">x</Button>
            </Modal.Header>
            <Modal.Body>
                <p>Do you want to delete user {props.user.username}</p>
            </Modal.Body>
            <Modal.Footer>
                <Button className="btn-blue" onClick={onsubmit}>
                    Yes
                </Button>
                <Button className="btn-clear">Cancel</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default ConfirmModal;
