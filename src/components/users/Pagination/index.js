import React from "react";

const Pagination = ({ totalCount, pageSize, currentPage, onPageChange }) => {
  
  const totalPageCount = Math.ceil(totalCount / pageSize);

  if (currentPage === 0) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  const NextRightButton = ({ disabled, eventButton }) => {
    return (
      <div className={`page-item ${disabled}`}>
        <button className="page-link btn-paging" onClick={eventButton}>
          &#x21E8;
        </button>
      </div>
    );
  };

  const NextLeftButton = ({ disabled, eventButton }) => {
    return (
      <div className={`page-item ${disabled}`}>
        <button className="page-link btn-paging" onClick={eventButton}>
          &#8678;
        </button>
      </div>
    );
  };

  return (
    <div className="table-top__pagination">
      <div className="table-top__pagination-prev">
        <NextLeftButton
          eventButton={onPrevious}
          disabled={currentPage === 1 ? "disabled" : ""}
        />
      </div>
      <ul className="table-top__pagination-number">
        {currentPage} / {totalPageCount}
      </ul>
      <div className="table-top__pagination-next">
        <NextRightButton
          eventButton={onNext}
          disabled={currentPage === totalPageCount ? "disabled" : ""}
        />
      </div>
    </div>
  );
};

export default Pagination;
