/**
 * @generated SignedSource<<cb3c5a3f38e1acfbfe8dc6b95dc2e7d2>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type FormModalGetUserQuery$variables = {||};
export type FormModalGetUserQuery$data = {|
  +user_aggregate: {|
    +aggregate: ?{|
      +count: number,
    |},
  |},
  +user: $ReadOnlyArray<{|
    +id: any,
    +password: string,
    +role: string,
    +username: string,
    +username_id: string,
  |}>,
|};
export type FormModalGetUserQuery = {|
  variables: FormModalGetUserQuery$variables,
  response: FormModalGetUserQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "user_aggregate",
    "kind": "LinkedField",
    "name": "user_aggregate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "user_aggregate_fields",
        "kind": "LinkedField",
        "name": "aggregate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "count",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "user",
    "kind": "LinkedField",
    "name": "user",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "password",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "role",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username_id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "FormModalGetUserQuery",
    "selections": (v0/*: any*/),
    "type": "query_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "FormModalGetUserQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "4bc4f244f33fa05bc843f7724c609f5f",
    "id": null,
    "metadata": {},
    "name": "FormModalGetUserQuery",
    "operationKind": "query",
    "text": "query FormModalGetUserQuery {\n  user_aggregate {\n    aggregate {\n      count\n    }\n  }\n  user {\n    id\n    password\n    role\n    username\n    username_id\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "f2982cd292871fa719f88e8c3d38c32a";

module.exports = ((node/*: any*/)/*: Query<
  FormModalGetUserQuery$variables,
  FormModalGetUserQuery$data,
>*/);
