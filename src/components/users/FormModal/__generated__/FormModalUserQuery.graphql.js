/**
 * @generated SignedSource<<748a8d81d0ad50fc8e7e30cfb2ff5ad9>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type FormModalUserQuery$variables = {||};
export type FormModalUserQuery$data = {|
  +user_aggregate: {|
    +aggregate: ?{|
      +count: number,
    |},
  |},
  +user: $ReadOnlyArray<{|
    +id: any,
    +password: string,
    +role: string,
    +username: string,
    +username_id: string,
  |}>,
|};
export type FormModalUserQuery = {|
  variables: FormModalUserQuery$variables,
  response: FormModalUserQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "user_aggregate",
    "kind": "LinkedField",
    "name": "user_aggregate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "user_aggregate_fields",
        "kind": "LinkedField",
        "name": "aggregate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "count",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "user",
    "kind": "LinkedField",
    "name": "user",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "password",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "role",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username_id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "FormModalUserQuery",
    "selections": (v0/*: any*/),
    "type": "query_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "FormModalUserQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "307a536fbba628dcf2fc7b1e28796754",
    "id": null,
    "metadata": {},
    "name": "FormModalUserQuery",
    "operationKind": "query",
    "text": "query FormModalUserQuery {\n  user_aggregate {\n    aggregate {\n      count\n    }\n  }\n  user {\n    id\n    password\n    role\n    username\n    username_id\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "86e8d1654fb23f1d54e7170694bfa08f";

module.exports = ((node/*: any*/)/*: Query<
  FormModalUserQuery$variables,
  FormModalUserQuery$data,
>*/);
