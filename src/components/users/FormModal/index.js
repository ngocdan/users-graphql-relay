import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { commitMutation } from "react-relay";
import environment from "../../../Enviroment";
import { createUserMutation } from "../../../graphql/mutations/createUser";
import { userMutation } from "../../../graphql/mutations/updateUser";
import { toast } from "react-toastify";
import useForm from "../../../hooks/useForm";
import { userValidate } from "../../../utils/validationUtils";

const FormModal = (props) => {
    const {
        form,
        errors,
        handleChange,
        handleSubmit,
        resetForm,
        initialValues,
    } = useForm(() => onSubmit(), userValidate);

    useEffect(() => {
        initialValues({
            ...props.user,
        });
    }, [props.user.id]);

    /**
     *  handle cancel form create or update user
     */
    const onCancel = () => {
        props.onHide();
        resetForm();
    };

    /**
     * handle save button create or update user
     */
    const onSubmit = () => {
        commitMutation(environment, {
            mutation: !props?.user.id ? createUserMutation : userMutation,
            variables: form,
            onCompleted: (response, errors) => {
                if (response) {
                    props.onHide();
                    props.retry();
                    resetForm();

                    toast.success(
                        `${
                            !props?.user.id
                                ? "Thêm user thành công!"
                                : "Đã cập nhật user!"
                        }`,
                        {
                            position: toast.POSITION.BOTTOM_RIGHT,
                        }
                    );
                }
            },
            onError: (err) => {
                toast.error(
                    `${
                        !props?.user.id
                            ? "Thêm user thất bại !"
                            : "Cập nhật user thất bại !"
                    }`,
                    {
                        position: toast.POSITION.BOTTOM_RIGHT,
                    }
                );
                console.error(err);
            },
        });
    };

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header className="d-flex justify-between align-items-center">
                <Modal.Title id="contained-modal-title-vcenter">
                    {props.user.id
                        ? `Edit user ${form?.username}`
                        : "Add New User"}
                </Modal.Title>
                <Button className="btn-close" onClick={onCancel}></Button>
            </Modal.Header>
            <Modal.Body>
                <form className="w-75 m-auto" autoComplete="off">
                    <div className="d-flex">
                        <Form.Label htmlFor="username_id" className="w-25 mr-2">
                            ID
                        </Form.Label>
                        <div className="w-100">
                            <Form.Control
                                type="text"
                                id="idUser"
                                name="username_id"
                                value={form?.username_id}
                                onChange={(e) =>
                                    handleChange(e.target.value, "username_id")
                                }
                                disabled={
                                    props.user?.username_id ? true : false
                                }
                            />
                            {errors?.username_id && (
                                <p className="text-danger m-0 error-input mt-1">
                                    {errors?.username_id?.message}
                                </p>
                            )}
                        </div>
                    </div>
                    <div className="mt-3 d-flex">
                        <Form.Label htmlFor="username" className="w-25 mr-2">
                            Username
                        </Form.Label>
                        <div className="w-100">
                            <Form.Control
                                type="text"
                                id="username"
                                name="username"
                                onChange={(e) =>
                                    handleChange(e.target.value, "username")
                                }
                                value={form?.username}
                            />
                            {errors?.username && (
                                <p className="text-danger m-0 error-input mt-1">
                                    {errors?.username?.message}
                                </p>
                            )}
                        </div>
                    </div>
                    <div className="mt-3 d-flex">
                        <Form.Label htmlFor="password" className="w-25 mr-2">
                            Password
                        </Form.Label>
                        <div className="w-100">
                            <Form.Control
                                type="password"
                                id="password"
                                name="password"
                                onChange={(e) =>
                                    handleChange(e.target.value, "password")
                                }
                                value={form?.password}
                            />
                            {errors?.password && (
                                <p className="text-danger m-0 error-input mt-1">
                                    {errors?.password?.message}
                                </p>
                            )}
                        </div>
                    </div>
                    <div className="mt-3 d-flex">
                        <Form.Label
                            htmlFor="confirm_password"
                            className="w-25 mr-2"
                        >
                            Confirm
                        </Form.Label>
                        <div className="w-100">
                            <Form.Control
                                type="password"
                                id="confirm_password"
                                name="confirm_password"
                                onChange={(e) =>
                                    handleChange(
                                        e.target.value,
                                        "confirm_password"
                                    )
                                }
                                value={form?.confirm_password}
                            />
                            {errors?.confirm_password && (
                                <p className="text-danger m-0 error-input mt-1">
                                    {errors?.confirm_password?.message}
                                </p>
                            )}
                        </div>
                    </div>

                    <Form.Group className="mt-4 d-flex">
                        <Form.Label className="mr-2 mb-0 w-25">Role</Form.Label>
                        <Form.Control
                            as="select"
                            name="role"
                            value={form?.role}
                            onChange={(e) =>
                                handleChange(e.target.value, "role")
                            }
                        >
                            <option value="Admin">Admin</option>
                            <option value="Manager">Manager</option>
                        </Form.Control>
                    </Form.Group>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button className="btn-blue" onClick={() => handleSubmit()}>
                    Save
                </Button>
                <Button className="btn-clear" onClick={onCancel}>
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default FormModal;
