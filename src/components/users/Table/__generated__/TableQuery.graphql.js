/**
 * @generated SignedSource<<80e6ceff612e00e81d00a4063f83e3f8>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type TableQuery$variables = {||};
export type TableQuery$data = {|
  +users_connection: {|
    +edges: $ReadOnlyArray<{|
      +node: {|
        +id: string,
        +role: string,
        +username: string,
        +username_id: string,
      |},
    |}>,
  |},
|};
export type TableQuery = {|
  variables: TableQuery$variables,
  response: TableQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "usersConnection",
    "kind": "LinkedField",
    "name": "users_connection",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "usersEdge",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "users",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "role",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "username",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "username_id",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "TableQuery",
    "selections": (v0/*: any*/),
    "type": "query_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "TableQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "e7f87b6467baa778b89036b42e0d25e6",
    "id": null,
    "metadata": {},
    "name": "TableQuery",
    "operationKind": "query",
    "text": "query TableQuery {\n  users_connection {\n    edges {\n      node {\n        id\n        role\n        username\n        username_id\n      }\n    }\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "f9cd9c9be207a2224e24170599bf45ac";

module.exports = ((node/*: any*/)/*: Query<
  TableQuery$variables,
  TableQuery$data,
>*/);
