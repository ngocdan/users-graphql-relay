import React, { useState } from "react";
import { Button, Table } from "react-bootstrap";
import ConfirmModal from "../ConfirmModal";
import FormModal from "../FormModal";
import Pagination from "../Pagination";

const TableUser = ({
    userList,
    reload,
    retry,
    totalElements,
    currentPage,
    pageSize,
    onPageChange,
}) => {
    const [modalShow, setModalShow] = useState();
    const [modalDeleteShow, setModalDeleteShow] = useState();
    const [user, setUser] = useState({});

    /**
     * handle show modal edit or add
     */
    const handleShowModal = (item) => {
        setModalShow(true);
        if (item) {
            setUser(item);
        } else {
            setUser({});
        }
    };

    /**
     * handle hide modal edit
     */
    const handleHideModal = () => {
        setModalShow(false);
        setModalDeleteShow(false);
        setUser({});
    };

    /**
     * handle show or hide modal delete
     */
    const handleShowModalDelete = (item) => {
        setModalDeleteShow(true);
        if (item) {
            setUser(item);
        } else {
            setUser({});
        }
    };

    return (
        <div className="pt-4">
            <div className="table-top mb-4">
                <div className="table-top__btn-add">
                    <Button
                        className="btn-blue w-100"
                        onClick={() => handleShowModal()}
                    >
                        Add new
                    </Button>
                </div>
                <Pagination
                    currentPage={currentPage}
                    totalCount={totalElements}
                    pageSize={pageSize}
                    onPageChange={onPageChange}
                />
            </div>
            <Table bordered size="sm">
                <thead>
                    <tr className="text-center">
                        <th>No</th>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Role</th>
                        {/* {userList?.length > 0 && <th></th>} */}
                    </tr>
                </thead>
                <tbody>
                    {userList?.length > 0 ? (
                        userList.map((userItem, i) => (
                            <tr className="text-center" key={`user-item-${i}`}>
                                <td>{i + 1}</td>
                                <td>{userItem?.username_id}</td>
                                <td
                                    className="cursor-pointer"
                                    onClick={() => handleShowModal(userItem)}
                                >
                                    {userItem?.username}
                                </td>
                                <td>{userItem?.role}</td>
                                <td className="d-flex justify-content-center">
                                    <Button
                                        variant="danger"
                                        className="w-50"
                                        onClick={() =>
                                            handleShowModalDelete(userItem)
                                        }
                                    >
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan={4} className="text-center py-4">
                                No display data.
                            </td>
                        </tr>
                    )}
                </tbody>
            </Table>
            <FormModal
                show={modalShow}
                user={user}
                userList={userList}
                onHide={handleHideModal}
                reload={reload}
                retry={retry}
            />
            <ConfirmModal
                show={modalDeleteShow}
                user={user}
                onHide={handleHideModal}
                reload={reload}
                retry={retry}
            />
        </div>
    );
};

export default TableUser;
