/**
 * @generated SignedSource<<09ddb4de6515d74db9aafc0a0f1d09f9>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type UserQuery$variables = {||};
export type UserQuery$data = {|
  +user_aggregate: {|
    +aggregate: ?{|
      +count: number,
    |},
  |},
  +user: $ReadOnlyArray<{|
    +id: any,
    +password: string,
    +role: string,
    +username: string,
    +username_id: string,
  |}>,
|};
export type UserQuery = {|
  variables: UserQuery$variables,
  response: UserQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "user_aggregate",
    "kind": "LinkedField",
    "name": "user_aggregate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "user_aggregate_fields",
        "kind": "LinkedField",
        "name": "aggregate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "count",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "user",
    "kind": "LinkedField",
    "name": "user",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "password",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "role",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "username_id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "UserQuery",
    "selections": (v0/*: any*/),
    "type": "query_root",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "UserQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "e8d6c4accfb4c8229cd9f0d3ccd7d546",
    "id": null,
    "metadata": {},
    "name": "UserQuery",
    "operationKind": "query",
    "text": "query UserQuery {\n  user_aggregate {\n    aggregate {\n      count\n    }\n  }\n  user {\n    id\n    password\n    role\n    username\n    username_id\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "8f5f001482c747c0b228707ff8f6767d";

module.exports = ((node/*: any*/)/*: Query<
  UserQuery$variables,
  UserQuery$data,
>*/);
