import React from "react";
import { UserContainer } from "../../containers";
import { QueryRenderer } from "react-relay";
import environment from "../../Enviroment";
import { queriesGetUserQuery } from "../../graphql/queries";

const Users = () => {
    return (
        <>
            <QueryRenderer
                environment={environment}
                query={queriesGetUserQuery}
                render={({ props, retry }) => {
                    return <UserContainer props={props} retry={retry} />;
                }}
            />
        </>
    );
};

export default Users;
