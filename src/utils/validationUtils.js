export const userValidate = (values) => {
  let errors = {};

  if (!values.username_id) {
    errors.username_id = {
      type: "required",
      message: "Please enter ID",
    };
  }

  if (!values.username) {
    errors.username = {
      type: "required",
      message: "Please enter username",
    };
  }

  if (!values.password) {
    errors.password = {
      type: "required",
      message: "Please enter password",
    };
  }

  if (!values.confirm_password) {
    errors.confirm_password = {
      type: "required",
      message: "Please enter confirm password",
    };
  } else if (values.password !== values.confirm_password) {
    errors.confirm_password = {
      type: "required",
      message: "Confirm password not match",
    };
  }

  return errors;
};
