// export const BASE_URL = "https://5e85e22044467600161c69bd.mockapi.io/api/user";
export const BASE_URL = process.env.REACT_APP_BASE_URL;
export const HASURA_KEY = process.env.REACT_APP_HASURA_KEY;
